from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('summernote/', include('django_summernote.urls')),
    path('verification/', include('verify_email_custom.urls')),
    path('admin/', admin.site.urls),

    path('resetpassword/', auth_views.PasswordResetView.as_view(template_name='passwordreset/passwordreset.html'),name='reset_password'),
    path('resetpasswordsent/', auth_views.PasswordResetDoneView.as_view(template_name='passwordreset/passwordresetsent.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(template_name='passwordreset/passwordresetconfirm.html'), name='password_reset_confirm'),
    path('passwordresetcomplete/', auth_views.PasswordResetCompleteView.as_view(template_name='passwordreset/passwordresetcomplete.html'), name='password_reset_complete'),

    path('', include('home.urls'), name='homepage'),
    path('author/', include('author.urls'), name='author'),
    path('books/', include('books.urls'), name='books'),
    path('blog/', include('blog.urls'), name='blog'),
    path('reviews/', include('reviews.urls'), name='reviews'),
    path('videos/', include('videos.urls'), name='videos'),
    path('contact/', include('contact.urls'), name='contact'),
    path('login/', include('login.urls'), name='login'),
    path('accounts/', include('accounts.urls'), name='accounts'),
    path('profile/', include('MyProfile.urls')),
] 
# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.conf.urls import (
handler400, handler403, handler404, handler500
)

# handler400 = 'my_app.views.bad_request'
# handler403 = 'my_app.views.permission_denied'
handler404 = 'login.views.urlError'
handler500 = 'login.views.ServerError'