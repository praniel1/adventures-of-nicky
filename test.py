def convertyoutubetoembed(string):
	if 'youtube.com' in string:
		strings = string.split('=')
		url, channel = strings[1].split('&')
		return url
	elif 'youtu.be' in string:
		strings = string.split('.be/')
		return strings[1]
	return string

print(convertyoutubetoembed('https://youtu.be/raN2SqxI9cY'))