from django.db import models

class Review(models.Model):
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=100)
	book_title = models.ForeignKey('books.Product', on_delete=models.CASCADE,null=True, blank=True)
	review_body = models.TextField()
	date = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return self.name