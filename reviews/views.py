from django.shortcuts import render, redirect
from django.http import HttpResponse
from . models import Review
from books.models import Product
from .forms import ReviewForm
from django.core.mail import send_mail
import requests
import json
from django.contrib import messages
from author.models import Author
from django.template.loader import render_to_string

def triggermail(form):
	try:
		authorobj = Author.objects.all().first()
		tomail = authorobj.review_mail
		sub = 'New review on Nicky'
		message = 'Hi!! There is a new Review by '+form.instance.name
		frommesg = 'nickyalex023@gmail.com'
		context = {
			'header_message': 'New Review for Adventures of Nicky',
			'name':form.instance.name,
			'email':form.instance.email,
			'subject':form.instance.book_title,
			'message':form.instance.review_body,
			'date':form.instance.date
		}
		html_message = render_to_string('reviews/emailnotify.html',context)
		send_mail(sub,message,frommesg,[tomail],html_message=html_message)
	except Exception as e:
		print(e)

def verifycapcha(capcha):
	cap_url = 'https://www.google.com/recaptcha/api/siteverify'
	cap_secret = '6Lei5EwbAAAAAKnF9L5xx10TA-tO2k9QVL5sc59T'
	capdata = {'secret':cap_secret,'response':capcha}
	cap_response = requests.post(url=cap_url,data=capdata)
	jsonresponse = json.loads(cap_response.text)
	return jsonresponse['success']

def index(request):
	if request.method == 'POST':
		form = ReviewForm(request.POST)
		capcha = request.POST.get('g-recaptcha-response')
		if not verifycapcha(capcha):
			messages.error(request,'Please verify capcha properly')
			return redirect('/reviews/')
		if form.is_valid():
			form.save()
			triggermail(form)
			return redirect('/reviews/')
		else:
			print(form.errors)
			return HttpResponse(form.errors)
	reviews = Review.objects.all().order_by('-pk')
	books = Product.objects.all()
	return render(request,'reviews/reviews.html',{'reviews':reviews,'books':books})

def viewreview(request, pk):
	review = Review.objects.get(pk=pk)
	return render(request,'reviews/view_review.html',{'review':review})

def deletereview(request, pk):
	review = Review.objects.get(pk=pk)
	review.delete()
	return redirect('/accounts/manage-reviews')