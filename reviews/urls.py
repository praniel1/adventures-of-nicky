from django.urls import path
from . import views
app_name='reviews'
urlpatterns = [
	path('', views.index, name='index'),
	path('delete/<int:pk>', views.deletereview, name='deletereview'),
	path('<int:pk>', views.viewreview, name='viewreview'),
]