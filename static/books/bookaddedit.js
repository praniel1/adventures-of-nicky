function checked(){
	$('.paperbackRow').show();
	$('input[name=paperback_quantity]').prop("required",true)
	$('input[name=paperback_price]').prop("required",true)
	$('select[name=shipping]').prop("required",true)
}
function notchecked(){
	$('.paperbackRow').hide();
	$('input[name="paperback_quantity"]').val('');
	$('input[name="paperback_price"]').val('');
	$('select[name="shipping"]').val('');
	$('input[name=paperback_quantity]').prop("required",false)
	$('input[name=paperback_price]').prop("required",false)
	$('select[name=shipping]').prop("required",false)
}
$(document).ready(function () {
	if($('#paper_back').prop('checked')){
		checked()		
	}
	else{
		notchecked()
	}
})
$('#paper_back').change(function () {
	if($(this).prop('checked')){
		checked()
	}
	else{
		notchecked()
	}
});