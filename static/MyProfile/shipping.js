// For Billing
$("#country_search").change(function(){
	countryid = document.getElementById('country_search').value
	$.ajax({
		url:"/profile/getstates?id="+countryid,
		success:function(data) {
			$("#state_search").html(data)
		}
	});
});
$("#state_search").change(function(){
	stateid = document.getElementById('state_search').value
	$.ajax({
		url:"/profile/getlocation?id="+stateid,
		success:function(data) {
			$("#location_search").html(data)
		}
	});
});

function pincodeSearch(){
	countrypk = $('#country_search').val()
	pincode = $('#pincode').val()
	if (countrypk.length < 1){
		alert('Please select country')
		return
	}
	$.ajax({
		url:"/profile/locationfrompin?countrypk="+countrypk+"&pincode="+pincode,
		success:function(data) {
			var data = JSON.parse(data)
			$("#state_search").html(data['state'])
			$("#location_search").html(data['location'])
		}
	});
}

// For Shipping 
$("#country_search_shipping").change(function(){
	countryid = document.getElementById('country_search_shipping').value
	$.ajax({
		url:"/profile/getstates?id="+countryid,
		success:function(data) {
			$("#state_search_shipping").html(data)
		}
	});
});
$("#state_search_shipping").change(function(){
	stateid = document.getElementById('state_search_shipping').value
	$.ajax({
		url:"/profile/getlocation?id="+stateid,
		success:function(data) {
			$("#location_search_shipping").html(data)
		}
	});
});

function pincodeSearchShipping(){
	countrypk = $('#country_search_shipping').val()
	pincode = $('#pincode_shipping').val()
	if (countrypk.length < 1){
		alert('Please select country')
		return
	}
	$.ajax({
		url:"/profile/locationfrompin?countrypk="+countrypk+"&pincode="+pincode,
		success:function(data) {
			var data = JSON.parse(data)
			$("#state_search_shipping").html(data['state'])
			$("#location_search_shipping").html(data['location'])
		}
	});
}

// Till here is shipping

$('#shippingsamecheckbox').change(function(){
	if($('#shippingsamecheckbox').prop('checked')){
		$('input[name=shippingFirstname]').val($('input[name=billingFirstname]').val())	
		$('input[name=shippingLastName]').val($('input[name=billingLastName]').val())	
		$('input[name=shippingEmail]').val($('input[name=billingEmail]').val())	
		$('input[name=shippingCompany]').val($('input[name=billingCompany]').val())	
		$('input[name=shippingAddress]').val($('input[name=billingAddress]').val())	
		$('input[name=shippingPhone]').val($('input[name=billingPhone]').val())	
		$('input[name=shippingFax]').val($('input[name=billingFax]').val())	
		// $('select[name=shippingLocation]').val($('select[name=billingLocation]').val())
		var text = $('#location_search').find(":selected").text();
		var val = $('#location_search').find(":selected").val();
		var option = '<option value="'+val+'" selected>'+text+'</option>'
		$('#location_search_shipping').append(option)

		var text = $('#state_search').find(":selected").text();
		var val = $('#state_search').find(":selected").val();
		var option = '<option value="'+val+'" selected>'+text+'</option>'
		$('#state_search_shipping').append(option)
	}
	else{
	}
})