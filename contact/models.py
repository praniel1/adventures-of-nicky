from django.db import models

class Message(models.Model):
	name = models.CharField(max_length=100)
	email = models.CharField(max_length=100, null=True, blank=True)
	subject = models.CharField(max_length=200)
	body = models.TextField()
	date = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return self.name