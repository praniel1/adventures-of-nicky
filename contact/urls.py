from django.urls import path
from . import views
app_name='contact'
urlpatterns = [
	path('', views.index, name='index'),
	path('view/<int:pk>', views.viewmessage, name='viewmessage'),
	path('delete/<int:pk>', views.deletemessage, name='deletemessage'),
]