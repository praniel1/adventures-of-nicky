from django.shortcuts import render,redirect
from django.http import HttpResponse
from home.errorfunction import convertformerror
from django.contrib import messages
from .forms import MessageForm
from .models import Message
from reviews.views import verifycapcha
from author.models import Author
from django.template.loader import render_to_string
from django.core.mail import send_mail

def triggermail(form):
	try:
		authorobj = Author.objects.all().first()
		tomail = authorobj.review_mail
		sub = 'New Message on Nicky'
		message = 'Hi!! There is a new Review by '+form.instance.name
		frommesg = 'nickyalex023@gmail.com'
		context = {
			'header_message': 'New Message for Adventures of Nicky',
			'name':form.instance.name,
			'email':form.instance.email,
			'subject':form.instance.subject,
			'message':form.instance.body,
			'date':form.instance.date
		}
		html_message = render_to_string('reviews/emailnotify.html',context)
		send_mail(sub,message,frommesg,[tomail],html_message=html_message)
	except Exception as e:
		print(e)


def index(request):
	if request.method == 'POST':
		form = MessageForm(request.POST)
		capcha = request.POST.get('g-recaptcha-response')
		if not verifycapcha(capcha):
			messages.error(request,'Please verify capcha properly')
			return redirect('/contact/')
		if form.is_valid():
			form.save()
			triggermail(form)
			messages.success(request,'Thank you, Your message has been noted')
			return redirect('/contact/')
		else:
			return HttpResponse(convertformerror(form.errors))
	return render(request,'contact/contact.html')


def viewmessage(request,pk):
	message = Message.objects.get(pk=pk)
	return render(request,'contact/viewmessage.html',{'message':message})

def deletemessage(request, pk):
	message = Message.objects.get(pk=pk)
	message.delete()
	return redirect('/accounts/manage-messages')