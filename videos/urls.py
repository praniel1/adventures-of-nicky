from django.urls import path
from . import views

app_name='videos'

urlpatterns = [
	path('', views.index, name='index'),
	path('addvideo', views.addvideo, name='addvideo'),
	path('editvideo/<int:pk>', views.editvideo, name='editvideo'),
	path('deletevideo/<int:pk>', views.deletevideo, name='deletevideo'),
	path('<str:slug>', views.viewvideo, name='viewvideo'),
]