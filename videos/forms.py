from django import forms
from .models import VideoPost

class VideoForm(forms.ModelForm):
	class Meta:
		model = VideoPost
		exclude = ['slug']
		fields = '__all__'