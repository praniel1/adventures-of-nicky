from django.db import models
from django.template.defaultfilters import slugify

class VideoPost(models.Model):
	video_title = models.CharField(max_length=100)
	slug = models.SlugField(max_length=700)
	video_description = models.TextField(null=True, blank=True)
	video_thumbnail = models.ImageField(upload_to='videos/thumbnail/')
	video_url = models.TextField(null=True,blank=True)
	video_file = models.FileField(upload_to='videos/file/',null=True,blank=True)
	def __str__(self):
		return self.video_title

	def save(self, *args, **kwargs):
		if not self.slug:
			original_slug = slugify(self.video_title)
			queryset = VideoPost.objects.all().filter(slug__iexact=original_slug).count()

			count = 1
			slug = original_slug
			while(queryset):
				slug = original_slug + '-' + str(count)
				count += 1
				queryset = VideoPost.objects.all().filter(slug__iexact=slug).count()

			self.slug = slug
		
		super(VideoPost, self).save(*args, **kwargs)