from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import VideoForm
from .models import VideoPost
from home.errorfunction import convertformerror

notaccesscontext={'message':'you cannot access this section'}
notloggedincontext={'message':'Please login with an admin accout'}

def convertyoutubetoembed(string):
	if 'youtube.com' in string:
		strings = string.split('=')
		url, channel = strings[1].split('&')
		return url
	elif 'youtu.be' in string:
		strings = string.split('.be/')
		return strings[1]
	return string


def index(request):
	videos = VideoPost.objects.all().order_by('-pk')
	return render(request,'videos/videolist.html',{'videos':videos})

def addvideo(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	request.POST._mutable = True
	url = convertyoutubetoembed(request.POST['video_url'])
	request.POST['video_url'] = 'https://www.youtube.com/embed/'+url
	form = VideoForm(request.POST, request.FILES)
	if form.is_valid():
		form.save()
		return redirect('/accounts/videos')
	else:
		return HttpResponse(convertformerror(form.errors))

def editvideo(request,pk):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	
	videoobj = VideoPost.objects.get(pk=pk)
	request.POST._mutable = True
	url = convertyoutubetoembed(request.POST['video_url'])
	request.POST['video_url'] = 'https://www.youtube.com/embed/'+url
	form = VideoForm(request.POST, request.FILES, instance=videoobj)
	if form.is_valid():
		form.save()
		return redirect('/accounts/videos')
	else:
		return HttpResponse(form.errors)

def deletevideo(request,pk):
	videoobj = VideoPost.objects.get(pk=pk)
	videoobj.delete()
	return redirect('/accounts/videos')

def viewvideo(request,slug):
	videoobj = VideoPost.objects.get(slug=slug)
	return render(request,'videos/videoview.html',{'video':videoobj})