from django.contrib import admin
from .models import BlogPost
from django_summernote.admin import SummernoteModelAdmin

class BlogPostAdmin(SummernoteModelAdmin):
	summernote_fields = ('post_content',)
	# exclude = ('slug', )

admin.site.register(BlogPost, BlogPostAdmin)