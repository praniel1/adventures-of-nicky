from django import forms
from .models import BlogPost
from django_summernote.widgets import SummernoteWidget

class BlogForm(forms.ModelForm):
	class Meta:
		model = BlogPost
		exclude = ['slug']
		widgets = {
			'post_content':SummernoteWidget(attrs={'summernote': {'width': '100%', 'height': '700px'}})
		}