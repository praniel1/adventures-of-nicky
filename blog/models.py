from django.db import models
from django.template.defaultfilters import slugify
from datetime import datetime

class BlogPost(models.Model):
	title = models.CharField(max_length=500)
	description = models.CharField(max_length=600, null=True, blank=True)
	post_image = models.ImageField(upload_to='blog/image/')
	slug = models.SlugField(max_length=700)
	date = models.DateTimeField(auto_now_add=True)
	post_content = models.TextField()

	def __str__(self):
		return self.title

	def save(self, *args, **kwargs):
		if not self.slug:
			original_slug = slugify(self.title)
			queryset = BlogPost.objects.all().filter(slug__iexact=original_slug).count()

			count = 1
			slug = original_slug
			while(queryset):
				slug = original_slug + '-' + str(count)
				count += 1
				queryset = BlogPost.objects.all().filter(slug__iexact=slug).count()

			self.slug = slug
		
		super(BlogPost, self).save(*args, **kwargs)