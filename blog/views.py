from django.shortcuts import render, redirect
from .forms import BlogForm
from django.http import HttpResponse
from .models import BlogPost
from home.errorfunction import convertformerror
from .emailfunction import sendemailtoall

notaccesscontext={'message':'you cannot access this section'}
notloggedincontext={'message':'Please login with an admin accout'}

def index(request):
	blogs = BlogPost.objects.all()
	return render(request, 'blog/blog.html',{'blogs':blogs})

def viewblog(request, slug):
	blogobj = BlogPost.objects.get(slug=slug)
	return render(request,'blog/viewblog.html',{'blog':blogobj})

def addBlogPost(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	blogform = BlogForm()
	if request.method == 'POST':
		blogform = BlogForm(request.POST, request.FILES)
		if blogform.is_valid():
			blogform.save()
			sendemailtoall(blogform,request)	
			return redirect('/accounts/manage-blog')
		else:
			print(blogform.errors)
			return HttpResponse(convertformerror(blogform.errors))
	return render(request, 'blog/addBlogPost.html', {'blogform':blogform})

def deleteBlogPost(request, slug):
	blogobj = BlogPost.objects.get(slug=slug)
	blogobj.delete()
	return redirect('/accounts/manage-blog')

def editBlogPost(request, slug):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	blogobj = BlogPost.objects.get(slug=slug)
	if request.method == 'POST':
		blogform = BlogForm(request.POST, request.FILES, instance=blogobj)
		if blogform.is_valid():
			blogform.save()
			return redirect('/accounts/manage-blog')
		else:
			return HttpResponse(blogform.errors)
	blogform = BlogForm(instance=blogobj)
	return render(request, 'blog/addBlogPost.html', {'blogform':blogform})