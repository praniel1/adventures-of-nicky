from django.urls import path
from . import views
app_name='blog'
urlpatterns = [
	path('', views.index, name='index'),
	path('add-post', views.addBlogPost, name='addBlogPost'),
	path('delete-post/<str:slug>', views.deleteBlogPost, name='deleteBlogPost'),
	path('edit-post/<str:slug>', views.editBlogPost, name='editBlogPost'),

	path('<str:slug>', views.viewblog, name='viewblog'),#this has to always be in the last
]