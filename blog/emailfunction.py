from django.core.mail import get_connection, EmailMultiAlternatives
from django.core.mail import send_mail, send_mass_mail
from accounts.models import User
from django.template.loader import render_to_string
from django.conf import settings



def send_mass_html_mail(datatuple, fail_silently=False, user=None, password=None, 
						connection=None):
	connection = connection or get_connection(
		username=user, password=password, fail_silently=fail_silently)
	messages = []
	for subject, text, html, from_email, recipient in datatuple:
		message = EmailMultiAlternatives(subject, text, from_email, recipient)
		message.attach_alternative(html, 'text/html')
		messages.append(message)
	return connection.send_messages(messages)

def sendemailtoall(blogform,request):
	mails = []
	usermails = User.objects.filter(is_active=True).filter(staff=False).filter(admin=False)
	if len(usermails) == 0:
		return
	for usermail in usermails:
		mails.append(usermail.email)
	domain = request.build_absolute_uri('/blog/'+blogform.instance.slug)
	html_message = render_to_string('emailtemplates/blogPromotionemail.html',{'blogform':blogform,'domain':domain})
	plain_message = 'Hi there is a new Blog in Adventures of nicky. Check it out at '+domain+blogform.instance.slug
	subject = 'Greetings from nicky'
	
	messages = []
	for mail in mails:
		message = (subject,plain_message,html_message,'nicky@gmail.com',[mail])
		messages.append(message)
	send_mass_html_mail(messages)