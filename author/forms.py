from django import forms
from .models import *
from django_summernote.widgets import SummernoteWidget

class AuthorForm(forms.ModelForm):
	class Meta:
		model = Author
		exclude = []
		widgets = {
			'description':SummernoteWidget()
		}

class SocialForm(forms.ModelForm):
	class Meta:
		model = SocialMedia
		fields = '__all__'