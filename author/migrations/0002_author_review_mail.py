# Generated by Django 3.2 on 2021-06-23 14:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('author', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='review_mail',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
