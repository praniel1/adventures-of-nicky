from django.db import models
from datetime import datetime
from django.utils import timezone

class Author(models.Model):
	author_image = models.ImageField(upload_to='author/image/', null=True, blank=True)
	description = models.TextField()
	review_mail = models.CharField(max_length=100, null=True, blank=True)
	updated = models.DateTimeField(default=datetime.now)
	def __str__(self):
		return 'author description'

	def save(self, *args, **kwargs):
		try:
			this = Author.objects.get(id=self.id)
			if this.author_image != self.author_image:
				this.author_image.delete()
		except: pass
		super(Author, self).save(*args, **kwargs)

class SocialMedia(models.Model):
	name = models.CharField(max_length=30)
	title = models.CharField(max_length=50)
	url = models.TextField()
	icon = models.ImageField(upload_to='author/socialmedia/', blank=True, null=True)

	def __str__(self):
		return self.name