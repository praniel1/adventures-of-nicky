from django.contrib import admin
from .models import *
from django_summernote.admin import SummernoteModelAdmin

class AuthorAdmin(SummernoteModelAdmin):
	summernote_fields = ('description',)

admin.site.register(Author, AuthorAdmin)

admin.site.register(SocialMedia)