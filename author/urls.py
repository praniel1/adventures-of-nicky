from django.urls import path
from . import views
app_name='author'
urlpatterns = [
	path('', views.index, name='index'),
	path('addauthordetail', views.addauthordetail, name='addauthordetail'),
	path('addEditSocial', views.addEditSocial, name='addEditSocial'),
	path('deletesocial/<int:pk>', views.deletesocial, name='deletesocial'),
]