from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from datetime import datetime
from home.errorfunction import convertformerror

def index(request):
	author = Author.objects.all().first()
	socials = SocialMedia.objects.all()
	return render(request, 'author/author.html', {'author':author,'socials':socials})

def addauthordetail(request):
	request.POST._mutable = True
	request.POST['updated'] = datetime.now()
	if Author.objects.all().first():
		authorform = AuthorForm(request.POST, request.FILES, instance=Author.objects.all().first())
	else:
		authorform = AuthorForm(request.POST, request.FILES)			
	if authorform.is_valid():
		authorform.save()
		return redirect('/accounts/')
	else:
		return HttpResponse(convertformerror(authorform.errors))

def addEditSocial(request):
	if request.POST['pk']:
		socialobj = SocialMedia.objects.get(pk=request.POST['pk'])
		form = SocialForm(request.POST, request.FILES, instance=socialobj)
	else:
		form = SocialForm(request.POST, request.FILES)
	if form.is_valid():
		form.save()
		return redirect('/accounts/')
	else:
		print(form.errors)
		return HttpResponse(form.errors)

def deletesocial(request, pk):
	socialobj = SocialMedia.objects.get(pk=pk)
	socialobj.delete()
	return redirect('/accounts/')