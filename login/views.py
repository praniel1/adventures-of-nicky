from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, get_user_model
from accounts.models import User
from django.contrib import messages
from django.shortcuts import redirect
from accounts.forms import UserForm
from verify_email_custom.email_handler import send_verification_email
from home.errorfunction import convertformerror

def index(request):
	if request.user.is_authenticated:
		return redirect("/profile/")
	if request.method == 'POST':
		user = authenticate(email=request.POST['email'], password=request.POST['password'])
		if user is not None:
			login(request, user)
			if request.GET.get('next'):
				return redirect(request.GET.get('next'))
			return redirect('/')
		else:
			url = request.get_full_path()
			messages.error(request,'Please check you credentials')
			return redirect(url)
	if request.GET.get('next'):
		messages.error(request,'Please login to continue')
	return render(request,'login/index.html')

def register(request):
	if request.method == 'POST':
		if request.POST['password'] != request.POST['confirm_password']:
			messages.error(request,'Passwords dont match')
			return redirect('/login/register')
		if User.objects.filter(email=request.POST['email']):
			messages.error(request,'Email already present')
			return redirect('/login/register')
		form = UserForm(request.POST)
		if form.is_valid():
			user = form.save(commit=False)
			password=form.cleaned_data['password']
			user.set_password(password)
			user.save()
			user = send_verification_email(request, form)
			if user:
				text='A verification Link has been sent to your email'
			else:
				text='There is some error sending the email'
			messages.success(request,text)
		else:
			print(form.errors)
			return HttpResponse(convertformerror(form.errors))
		return redirect('/login')
	return render(request,'login/register.html')

def urlError(request,exception):
	return render(request, 'login/errors/404error.html')

def ServerError(request):
	return render(request, 'login/errors/500error.html')