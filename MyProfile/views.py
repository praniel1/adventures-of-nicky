from django.shortcuts import render, redirect
from django.http import HttpResponse
from books.models import *
from home.errorfunction import convertformerror
from django.contrib.auth.decorators import login_required
from accounts.models import *
from . import otherfunctions
from django.contrib import messages
from .emailorderfunction import sendorderemail

@login_required(login_url='/login')
def index(request):
	if request.method == 'POST':
		user = request.user
		user.first_name = request.POST['first_name']
		user.last_name = request.POST['last_name']
		user.save()
		return redirect('/profile')
	return render(request, 'MyProfile/index.html')

@login_required(login_url='/login')
def myorders(request):
	myorders = Orders.objects.filter(user=request.user)
	return render(request, 'MyProfile/myorders.html', {'myorders':myorders})

@login_required(login_url='/login')
def vieworder(request,pk):
	order = Orders.objects.get(pk=pk)
	return render(request, 'MyProfile/vieworder.html',{'order':order})

@login_required(login_url='/login')
def getcart(request):
	cartitems = CartItem.objects.filter(user=request.user)
	return render(request, 'MyProfile/mycart.html',{'cartitems':cartitems})

@login_required(login_url='/login')
def updatecart(request):
	cartitem = CartItem.objects.get(pk=request.POST['pk'])
	if cartitem.itemtype == 'paperback':
		newquantity = int(request.POST['quantity'])
		paperback_quantity = cartitem.product.paperback_quantity
		if newquantity > paperback_quantity:
			messages.error(request,'Quantity more than available')
			return redirect('/profile/cart')
	cartitem.quantity = request.POST['quantity']
	cartitem.save()
	return redirect('/profile/cart')

@login_required(login_url='/login')
def deletecartitem(request,pk):
	cartitem = CartItem.objects.get(pk=pk)
	cartitem.delete()
	return redirect('/profile/cart')

@login_required(login_url='/login')
def checkout(request):
	cartitems = CartItem.objects.filter(user=request.user)
	return render(request,'MyProfile/checkout.html',{'cartitems':cartitems})

@login_required(login_url='/login')
def selectaddress(request):
	addresses = CustomerAddress.objects.filter(user=request.user)
	return render(request,'MyProfile/selectaddress.html',{'addresses':addresses})

@login_required(login_url='/login')
def deleteaddress(request, pk):
	address = CustomerAddress.objects.get(pk=pk)
	address.delete()
	return redirect('/profile/selectaddress')

# @login_required(login_url='/login')
def shippingdetails(request):
	if request.method == 'POST':
		form = otherfunctions.handleshippingaddress(request)
		if form.is_valid():
			newform = form.save()
			if not request.user.is_authenticated:
				request.session['address'] = newform.pk
			return redirect('/profile/payment')
		else:
			print(form.errors)
			return HttpResponse(convertformerror(form.errors))
	else:
		context = {'countries':Country.objects.all()}
		if not request.user.is_authenticated:
			pk = request.session['addresspk'] = otherfunctions.saveGuestOrdertoCart(request.GET['bookpath'])
			request.session['cartitem'] = pk
			context['cartitems']=CartItem.objects.filter(pk=pk)
		else:
			context['cartitems']=CartItem.objects.filter(user=request.user)
		if request.GET.get('address'):
			context['address'] = CustomerAddress.objects.get(pk=request.GET.get('address'))
		return render(request, 'MyProfile/shippingdetails.html', context)

# @login_required(login_url='/login')
def payment(request):
	context = {'total':otherfunctions.gettotalamount(request)}
	if not request.user.is_authenticated:
		pk = request.session['addresspk']
		request.session['cartitem'] = pk
		context['cartitems'] =CartItem.objects.filter(pk=pk)
	else:
		context['cartitems'] =CartItem.objects.filter(user=request.user)
	return render(request, 'MyProfile/payment.html',context) 


def paymentcomplete(request):
	if request.user.is_authenticated:
		shippingobj = CustomerAddress.objects.filter(user=request.user).get(active=True)
	else:
		shippingobj = CustomerAddress.objects.get(pk=request.session['address'])
	if request.user.is_authenticated:
		cartitems = CartItem.objects.filter(user=request.user)
	else:
		cartitems = CartItem.objects.filter(pk=request.session['cartitem'])
	savedorders = otherfunctions.cartToOrder(cartitems,shippingobj)
	sendorderemail(savedorders,shippingobj.billingEmail,shippingobj.shippingEmail, request)
	return HttpResponse('saved')

def ordermessage(request):
	# destroy sessions
	return render(request,'MyProfile/ordermessage.html')