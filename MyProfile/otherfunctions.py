from books.models import *
from accounts.forms import *

def gettotalamount(request):
	if request.user.is_authenticated:
		items = CartItem.objects.filter(user=request.user)
	else:
		items = CartItem.objects.filter(pk=request.session['cartitem'])
	total = 0
	for item in items:
		total += item.calculatetotal()
	return total

def handleshippingaddress(request):
	if request.user.is_authenticated:
		request.POST._mutable = True
		request.POST['user'] = request.user
		request.POST['active'] = True
	shippingpk = request.POST.get('shippingpk')
	if shippingpk != '':
		form = AddressForm(request.POST, instance=CustomerAddress.objects.get(pk=shippingpk))
	else:
		form = AddressForm(request.POST)
	if request.user.is_authenticated:
		addressobj = CustomerAddress.objects.filter(user=request.user)
		addressobj = addressobj.filter(active=True)
		if len(addressobj) > 0:
			for add in addressobj:
				add.active = False
				add.save()
	return form

def cartToOrder(cartitems,shippingobj):
	savedorders = []
	for cartitem in cartitems:
		order = Orders()
		order.address = shippingobj
		if cartitem.user:
			order.user = cartitem.user
		order.product = cartitem.product
		order.itemtype = cartitem.itemtype
		order.quantity = cartitem.quantity
		order.price_total = cartitem.calculatetotal()
		if order.itemtype == 'paperback':
			book = cartitem.product
			quantity = book.paperback_quantity
			quantity = quantity-order.quantity
			book.paperback_quantity = quantity
			book.save()
		order.save()
		savedorders.append(order)
		cartitem.delete()
	return savedorders

def saveGuestOrdertoCart(path):
	bookname = path.replace('/books/addtocart/','')
	slug = bookname.split('?ptype')[0]
	ptype = path.split('ptype=')[1]
	book = Product.objects.get(slug=slug)
	cartitem = CartItem()
	cartitem.product = book
	cartitem.itemtype = ptype
	cartitem.save()
	return cartitem.pk