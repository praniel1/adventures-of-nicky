from accounts.models import User
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
import os


def sendorderemail(orderitems,billingemail,shippingemail, request):
	subject = 'Order Summary'
	html_message = render_to_string('emailtemplates/ordermail.html',
									{'orderitems':orderitems})
	plain_message = strip_tags(html_message)
	from_email = 'nicky@gmail.com'
	send_mail(subject, plain_message, from_email, [billingemail,shippingemail], 
			html_message=html_message)
	

	if len(orderitems) == 1:
		order = orderitems[0]
		if not order.user:
			if order.itemtype != 'paperback':
				domain = request.build_absolute_uri('/')
				html_message = render_to_string('emailtemplates/orderdeditem.html',{'order':order,'domain':domain})
				mail = EmailMessage('Your Order',html_message,
					'nicky@gmail.com',[billingemail,shippingemail])
				if not order.user:
					book = order.product
					if order.itemtype == 'audio':
						fullpath = os.path.join(settings.MEDIA_ROOT, book.audio.name)
					elif(order.itemtype == 'ebook'):
						fullpath = os.path.join(settings.MEDIA_ROOT, book.ebook.name)
					else:
						return
				mail.attach_file(fullpath)
				mail.content_subtype='html'
				mail.send()