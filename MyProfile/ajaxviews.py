from accounts.models import *
from django.http import HttpResponse

def getstates(request):
	pk = request.GET['id']
	searchresult = State.objects.filter(country=Country.objects.get(pk=pk)).order_by('statename')
	string = '<option value="">---select state---</option>'
	for search in searchresult:
		string += '<option value='+str(search.pk)+'>'+search.statename+'</option>'
	return HttpResponse(string)

def getlocation(request):
	pk = request.GET['id']
	searchresult = Location.objects.filter(state=State.objects.get(pk=pk)).order_by('pincode')
	string = '<option value="">---select location---</option>'
	for search in searchresult:
		string += '<option value='+str(search.pk)+'>'+str(search)+'</option>'
	return HttpResponse(string)

def locationfrompin(request):
	countrypk = request.GET['countrypk']
	pincode = request.GET['pincode']
	state = State.objects.get(country=Country.objects.get(pk=countrypk))
	location = Location.objects.filter(state=state)
	location = location.get(pincode=pincode)
	data = {
		'state':'<option value='+str(state.pk)+'>'+str(state.statename)+'</option>',
		'location':'<option value='+str(location.pk)+'>'+str(location.pincode)+' '+location.city+'</option>'
	}
	data = json.dumps(data)
	return HttpResponse(data)