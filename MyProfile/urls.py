from django.urls import path
from . import views
from . import ajaxviews

app_name='MyProfile'

urlpatterns = [
	path('', views.index, name='index'),
	path('myorders', views.myorders, name='myorders'),
	path('vieworder/<int:pk>', views.vieworder, name='vieworder'),
	path('cart', views.getcart, name='cart'),
	path('updatecart', views.updatecart, name='updatecart'),
	path('deletecartitem/<int:pk>', views.deletecartitem, name='deletecartitem'),
	path('checkout', views.checkout, name='checkout'),
	path('selectaddress', views.selectaddress, name='selectaddress'),
	path('deleteaddress/<int:pk>', views.deleteaddress, name='deleteaddress'),
	path('shippingdetails', views.shippingdetails, name='shippingdetails'),

	path('getstates', ajaxviews.getstates, name='getstates'),
	path('getlocation', ajaxviews.getlocation, name='getlocation'),
	path('locationfrompin', ajaxviews.locationfrompin, name='locationfrompin'),

	path('payment', views.payment, name='payment'),
	path('paymentcomplete', views.paymentcomplete, name='paymentcomplete'),
	path('ordermessage', views.ordermessage, name='ordermessage'),
]