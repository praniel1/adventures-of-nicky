from django.apps import AppConfig


class VerifyEmailConfig(AppConfig):
    name = 'verify_email_custom'

    def ready(self):
        # print('importing signals')
        import verify_email_custom.signals
