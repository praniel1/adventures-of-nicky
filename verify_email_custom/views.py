from .app_configurations import GetFieldFromSettings
from .confirm import _verify_user
from django.contrib import messages
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib import messages
from .token_manager import error_type
from .email_handler import resend_verification_email

success_msg = GetFieldFromSettings().get('verification_success_msg')
failed_msg = GetFieldFromSettings().get('verification_failed_msg')

failed_template = GetFieldFromSettings().get('verification_failed_template')
success_template = GetFieldFromSettings().get('verification_success_template')
link_expired_template = GetFieldFromSettings().get('link_expired_template')


def verify_user_and_activate(request, useremail, usertoken):
    verified = _verify_user(useremail, usertoken)
    if verified is True:
        messages.error(request,'Congratulations!! Your account is activated')
        return redirect('/login/')
    elif verified == error_type.expired:
        messages.error(request,'link has expired :Please Request a new one')
        url = request.build_absolute_uri()
        urls = url.split('verify-email')
        url = urls[0]+'verify-email/request-new-link'+urls[1]
        # return redirect('/login/request-new-link')
        return render(request,'login/requestnewlink.html',{'url':url})
    elif verified == error_type.tempered:
        messages.error(request,'link has been modified :Please Request a new one')
        return redirect('/login/')
    elif verified == error_type.mre:
        messages.error(request,'Maximum number of request link reached')
        return redirect('/login/')
    else:
        messages.error(request,'There has been some error with this link')
        return redirect('/login/')


def request_new_link(request, useremail, usertoken):
    status = resend_verification_email(request, useremail, usertoken)
    if status:
        messages.success(request,'Another verification Link has been sent to your email')
        return redirect('/login/')
    elif status == error_type.mre:
        return render(
            request,
            template_name=failed_template,
            context={
                'msg': 'You have exceeded the maximum verification requests! Contact admin.',
                'status': 'Maxed out!',
            }
        )
    else:
        return render(
            request,
            template_name=failed_template,
            context={
                'msg': failed_msg,
                'minor_msg': 'Cannot send verification link to you! contact admin.',
                'status': 'Verification Failed!',
            }
        )
