from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

def sendmailupdate(form):
	subject = 'Order Update'
	html_message = render_to_string('emailtemplates/updateorder.html',{'form':form})
	plain_message = strip_tags(html_message)
	from_email = 'nicky@gmail.com'
	send_mail(subject, plain_message, from_email, 
		[form.address.billingEmail,form.address.shippingEmail], 
		html_message=html_message)