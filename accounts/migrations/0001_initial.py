# Generated by Django 3.2 on 2021-06-01 13:08

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100, unique=True)),
                ('is_active', models.BooleanField(default=True)),
                ('staff', models.BooleanField(default=False)),
                ('admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CardType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('countryname', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('statename', models.CharField(max_length=50)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.country')),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pincode', models.IntegerField()),
                ('city', models.CharField(max_length=50)),
                ('state', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.state')),
            ],
        ),
        migrations.CreateModel(
            name='CustomerAddress',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('billingFirstname', models.CharField(blank=True, max_length=100, null=True)),
                ('billingLastName', models.CharField(blank=True, max_length=100, null=True)),
                ('billingEmail', models.CharField(max_length=200)),
                ('billingCompany', models.CharField(blank=True, max_length=200, null=True)),
                ('billingAddress', models.CharField(blank=True, max_length=200, null=True)),
                ('billingPhone', models.CharField(blank=True, max_length=15, null=True)),
                ('billingFax', models.CharField(blank=True, max_length=15, null=True)),
                ('shippingFirstname', models.CharField(blank=True, max_length=100, null=True)),
                ('shippingLastName', models.CharField(blank=True, max_length=100, null=True)),
                ('shippingEmail', models.CharField(max_length=200)),
                ('shippingCompany', models.CharField(blank=True, max_length=200, null=True)),
                ('shippingAddress', models.CharField(blank=True, max_length=200, null=True)),
                ('shippingPhone', models.CharField(blank=True, max_length=15, null=True)),
                ('shippingFax', models.CharField(blank=True, max_length=15, null=True)),
                ('active', models.BooleanField(default=True)),
                ('date_created', models.DateTimeField(default=datetime.datetime.now)),
                ('billingLocation', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='billing', to='accounts.location')),
                ('shippingLocation', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='shipping', to='accounts.location')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.RESTRICT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
