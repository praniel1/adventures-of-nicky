from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from .ModelManager import UserManager
from datetime import datetime

class User(AbstractBaseUser):
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	email = models.CharField(max_length=100, unique=True)
	is_active = models.BooleanField(default=True)
	staff = models.BooleanField(default=False)
	admin = models.BooleanField(default=False)
	USERNAME_FIELD = 'email'
	objects = UserManager()
	@property
	def is_staff(self):
		return self.staff
	@property
	def is_admin(self):
		return self.admin
	def has_perm(self, perm, obj=None):
		return True
	def has_module_perms(self, app_lable):
		return True


class Country(models.Model):
	countryname = models.CharField(max_length=30)
	def __str__(self):
		return self.countryname

class State(models.Model):
	statename = models.CharField(max_length=50)
	country = models.ForeignKey(Country, on_delete=models.CASCADE)
	def __str__(self):
		return self.statename

class Location(models.Model):
	pincode = models.IntegerField()
	city = models.CharField(max_length=50)
	state = models.ForeignKey(State, on_delete=models.CASCADE)	
	def __str__(self):
		return str(self.pincode)+" "+self.city

class CardType(models.Model):
	card_name = models.CharField(max_length=50)

	def __str__(self):
		return self.card_name

class CustomerAddress(models.Model):
	billingFirstname = models.CharField(max_length=100, blank=True, null=True)
	billingLastName = models.CharField(max_length=100, blank=True, null=True)
	billingEmail = models.CharField(max_length=200)
	billingCompany = models.CharField(max_length=200, blank=True, null=True)
	billingAddress = models.CharField(max_length=200, blank=True, null=True)
	billingPhone = models.CharField(max_length=15, blank=True, null=True)
	billingFax = models.CharField(max_length=15, blank=True, null=True)
	billingLocation = models.ForeignKey(Location, on_delete=models.RESTRICT, related_name='billing')

	shippingFirstname = models.CharField(max_length=100, blank=True, null=True)
	shippingLastName = models.CharField(max_length=100, blank=True, null=True)
	shippingEmail = models.CharField(max_length=200)
	shippingCompany = models.CharField(max_length=200, blank=True, null=True)
	shippingAddress = models.CharField(max_length=200, blank=True, null=True)
	shippingPhone = models.CharField(max_length=15, blank=True, null=True)
	shippingFax = models.CharField(max_length=15, blank=True, null=True)
	shippingLocation = models.ForeignKey(Location, on_delete=models.RESTRICT, related_name='shipping')

	active = models.BooleanField(default=True)
	# CC_type = models.ForeignKey('CardType', on_delete=models.RESTRICT)
	# CC_number = models.CharField(max_length=30)
	# CC_Expires = models.CharField(max_length=12)
	date_created = models.DateTimeField(default=datetime.now)

	user = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)

	def __str__(self):
		return self.billingEmail