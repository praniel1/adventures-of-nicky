from django.urls import path
from . import views

app_name='accounts'

urlpatterns = [
	path('', views.index, name='index'),
	path('manage-blog', views.manageBlog, name='manageBlog'),
	path('manage-taxes', views.manageTaxes, name='manageTaxes'),
	path('deletetax/<int:pk>', views.deletetax, name='deletetax'),
	path('manage-books', views.manageBooks, name='manageBooks'),
	path('deletebook/<int:pk>', views.deletebook, name='deletebook'),
	path('manage-reviews', views.manageReviews, name='manageReviews'),
	path('manage-messages', views.manageMessages, name='manageMessages'),
	path('videos', views.manageVideos, name='manageVideos'),
	path('shipping', views.shipping, name='shipping'),
	path('deleteshipping/<int:pk>', views.deleteshipping, name='deleteshipping'),
	path('orders', views.orders, name='orders'),
	path('updateorder/<int:pk>',views.updateorder, name='updateorder'),
	path('users', views.manageUsers, name='manageUsers'),
]