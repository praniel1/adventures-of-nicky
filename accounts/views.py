from django.shortcuts import render, redirect
from django.http import HttpResponse
from author.forms import AuthorForm
from author.models import *
from blog.models import BlogPost
from books.models import *
from books.forms import *
from reviews.models import *
from contact.models import *
from videos.models import *
from home.errorfunction import convertformerror
from .otherfunctions import sendmailupdate

notaccesscontext={'message':'you cannot access this section'}
notloggedincontext={'message':'Please login with an admin accout'}


def index(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	context = {
		'authorform':AuthorForm(instance=Author.objects.all().first()),
		'socials':SocialMedia.objects.all(),
	}
	return render(request,'accounts/manage_author.html',context)

def manageBlog(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	blogs = BlogPost.objects.all()
	return render(request,'accounts/manage_blog.html', {'blogs':blogs})

def manageTaxes(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	if request.method == 'POST':
		if request.POST['pk']:
			form = TaxesForm(request.POST, instance=Taxes.objects.get(pk=request.POST['pk']))
		else:
			form = TaxesForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/accounts/manage-taxes')
		else:
			return HttpResponse(form.errors)
	taxes = Taxes.objects.all()
	return render(request,'accounts/manage_taxes.html',{'taxes':taxes})

def deletetax(request,pk):
	tax = Taxes.objects.get(pk=pk)
	tax.delete()
	return redirect('/accounts/manage-taxes')

def manageBooks(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	books = Product.objects.all()
	return render(request,'accounts/manage_books.html',{'books':books})

def deletebook(request,pk):
	book = Product.objects.get(pk=pk)
	return HttpResponse(book)
	book.delete()
	return redirect('/accounts/manage-books/')

def manageReviews(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	reviews = Review.objects.all()
	return render(request,'accounts/manage_reviews.html',{'reviews':reviews})

def manageMessages(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	messages = Message.objects.all()
	return render(request,'accounts/manage_messages.html',{'messages':messages})

def manageVideos(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	videos = VideoPost.objects.all()
	return render(request,'accounts/manage_videos.html',{'videos':videos})

def shipping(request):
	if request.method == 'POST':
		if request.POST['pk']:
			pk = request.POST['pk']
			form = ShippingForm(request.POST, instance=ShippingFee.objects.get(pk=pk))
		else:
			form = ShippingForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('/accounts/shipping')
		else:
			return HttpResponse(convertformerror(form.errors))
	shippingfees = ShippingFee.objects.all()
	return render(request, 'accounts/shipping.html',{'shippingfees':shippingfees})

def deleteshipping(request, pk):
	shipping = ShippingFee.objects.get(pk=pk)
	shipping.delete()
	return redirect('/accounts/shipping')

def orders(request):
	orders = Orders.objects.all()
	return render(request,'accounts/manage_orders.html',{'orders':orders})

def updateorder(request,pk):
	if request.method == 'POST':
		form = OrderForm(request.POST, instance=Orders.objects.get(pk=pk))
		if form.is_valid():
			saved = form.save()
			if form.instance.itemtype == 'paperback':
				sendmailupdate(Orders.objects.get(pk=pk))
			return redirect('/accounts/updateorder/'+str(saved.pk))
		else:
			return HttpResponse(convertformerror(form.errors))
	order = Orders.objects.get(pk=pk)
	return render(request, 'accounts/updateOrder.html',{'order':order})

def manageUsers(request):
	if request.method == 'POST':
		user = User.objects.get(pk=request.POST['pk'])
		if request.POST.get('is_active'):
			user.is_active = True
		else:
			user.is_active = False
		user.save()
		return redirect('/accounts/users')
	users = User.objects.all()
	return render(request,'accounts/manage_users.html',{'users':users})