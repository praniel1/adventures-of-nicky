from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import *

user = get_user_model()

admin.site.register(user)
admin.site.register(Country)
admin.site.register(Location)
admin.site.register(CardType)

class AddressAdmin(admin.ModelAdmin):
	list_display = ['billingEmail','active']
admin.site.register(CustomerAddress, AddressAdmin)
admin.site.register(State)