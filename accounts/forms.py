from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from accounts.models import User

class AddressForm(forms.ModelForm):
	class Meta:
		exclude = ['date_created']
		model = CustomerAddress
		fields = '__all__'

class UserForm(forms.ModelForm):
	first_name = forms.CharField()
	last_name = forms.CharField()
	email = forms.EmailField()
	class Meta:
		model = User
		fields = ('first_name','last_name','email','password')
		# fields = '__all__'


# class UserForm(forms.Form):
# 	first_name = forms.CharField(max_length=100)
# 	last_name = forms.CharField(max_length=100)
# 	email = forms.CharField(max_length=100)
# 	password = forms.CharField(label='Confirm password', widget=forms.PasswordInput)
# 	def save(self, commit=True):
# 		user = User.objects.create_user(
# 			self.cleaned_data['first_name'],
# 			self.cleaned_data['last_name'],
# 			self.cleaned_data['email'],
# 			self.cleaned_data['password']
# 		)
# 		return user