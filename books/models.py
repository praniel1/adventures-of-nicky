from django.db import models
from django.template.defaultfilters import slugify
from accounts.models import User
class ShippingFee(models.Model):
	each = models.DecimalField(max_digits=10, decimal_places=2)
	additional = models.DecimalField(max_digits=10, decimal_places=2)
	def __str__(self):
		return str(self.each)+ " "+str(self.additional)

class Taxes(models.Model):
	name = models.CharField(max_length=50)
	percentage = models.DecimalField(max_digits=10, decimal_places=2)
	description = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.name

class Product(models.Model):
	name = models.TextField()
	description = models.TextField(null=True, blank=True)
	active = models.BooleanField(default=True)
	slug = models.SlugField(max_length=700)
	cover_image = models.ImageField(upload_to='book/cover/')
	sample_book = models.FileField(upload_to='book/samples/', null=True, blank=True)
	taxes = models.ManyToManyField(Taxes,blank=True)

	audio = models.FileField(upload_to='book/audio', null=True, blank=True)
	audio_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

	ebook = models.FileField(upload_to='book/audio', null=True, blank=True)
	ebook_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
	
	paperback = models.BooleanField(default=True)
	paperback_quantity = models.IntegerField(null=True, blank=True)
	paperback_price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
	shipping = models.ForeignKey(ShippingFee, null=True, blank=True, on_delete=models.RESTRICT)

	def save(self, *args, **kwargs):
		if not self.slug:
			original_slug = slugify(self.name)
			queryset = Product.objects.all().filter(slug__iexact=original_slug).count()

			count = 1
			slug = original_slug
			while(queryset):
				slug = original_slug + '-' + str(count)
				count += 1
				queryset = Product.objects.all().filter(slug__iexact=slug).count()

			self.slug = slug
		
		super(Product, self).save(*args, **kwargs)

	def __str__(self):
		return self.name 


class Orders(models.Model):
	user = models.ForeignKey(User, on_delete=models.RESTRICT,null=True, blank=True)
	address = models.ForeignKey('accounts.CustomerAddress', on_delete=models.RESTRICT)
	product = models.ForeignKey(Product, on_delete=models.RESTRICT)
	itemtype = models.CharField(max_length=10)
	quantity = models.IntegerField()
	price_total = models.DecimalField(max_digits=10, decimal_places=2)
	date_ordered = models.DateTimeField(auto_now_add=True)
	courier_details = models.TextField(blank=True, null=True)
	shipped_date = models.DateTimeField(blank=True, null=True)
	completed = models.BooleanField(default=False)

	def __str__(self):
		return str(self.address)

class CartItem(models.Model):
	user = models.ForeignKey('accounts.User', on_delete=models.CASCADE, null=True,blank=True)
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	quantity = models.IntegerField(default=1)
	date_added = models.DateTimeField(auto_now_add=True)
	itemtype = models.CharField(max_length=10)
	def __str__(self):
		return str(self.product)

	def getprice(self):
		if self.itemtype == 'ebook':
			price = self.product.ebook_price
		elif self.itemtype == 'paperback':
			price = self.product.paperback_price
		else:
			price = self.product.audio_price
		return price*self.quantity

	def gettax(self):
		price = self.getprice()
		taxes_total = 0
		for taxperc in self.product.taxes.all():
			tax = (price*taxperc.percentage)/100
			taxes_total += tax
		taxes_total = round(taxes_total,2)
		return taxes_total

	def getshipping(self):
		if self.itemtype != 'paperback':
			return 0 # else it is paperback
		if self.quantity == 1:
			return self.product.shipping.each
		else:
			totalshipping = 0
			for i in range(0,self.quantity):
				if i == 0:
					totalshipping += self.product.shipping.each
				else:
					totalshipping += self.product.shipping.additional
			return totalshipping

	def calculatetotal(self):
		price = self.getprice()
		gettax = self.gettax()
		shipping = self.getshipping()
		return (price+gettax+shipping)