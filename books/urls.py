from django.urls import path
from . import views

app_name='books'

urlpatterns = [
	path('', views.index, name='index'),
	path('addbook',views.addbook, name='addbook'),
	path('editbook/<str:slug>', views.editbook, name='editbook'),
	path('view/<str:slug>', views.viewbook, name='viewbook'),
	path('addtocart/<str:slug>', views.addtocart, name='addtocart'),
]