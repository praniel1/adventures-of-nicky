from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from home.errorfunction import convertformerror
from .forms import *
from decimal import *
from django.contrib.auth.decorators import login_required

notaccesscontext={'message':'you cannot access this section'}
notloggedincontext={'message':'Please login with an admin accout'}

def index(request):
	books = Product.objects.all().filter(active=True)
	return render(request, 'books/books.html', {'books':books})

def addbook(request):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	if request.method == 'POST':
		form = BookForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/accounts/manage-books')
		else:
			return HttpResponse(convertformerror(form.errors))
	taxes = Taxes.objects.all()
	shipping = ShippingFee.objects.all()
	return render(request, 'books/add_book.html',{'taxes':taxes,'shippings':shipping})

def editbook(request,slug):
	if not request.user.is_authenticated:
		return render(request,'login/problemurl.html',notloggedincontext)
	if not request.user.is_admin:		
		return render(request,'login/problemurl.html',notaccesscontext)
	book = Product.objects.get(slug=slug)
	if request.method == 'POST':
		form = BookForm(request.POST, request.FILES, instance=book)
		if form.is_valid():
			form.save()
			return redirect('/accounts/manage-books')
		else:
			return HttpResponse(convertformerror(form.errors))
	taxes = Taxes.objects.all()
	shipping = ShippingFee.objects.all()
	return render(request, 'books/edit_book.html',{'taxes':taxes,'book':book,'shippings':shipping})

def viewbook(request, slug):
	book = Product.objects.get(slug=slug)
	return render(request, 'books/viewbook.html',{'book':book})

@login_required(login_url='/login')
def addtocart(request,slug):
	book = Product.objects.get(slug=slug)
	cartitems = CartItem.objects.filter(product=book).filter(user=request.user)
	cartitems = cartitems.filter(itemtype = request.GET['ptype'])
	if len(cartitems) > 0: # if item present increase quantity
		cartitem = cartitems[0]
		quantity = cartitem.quantity
		cartitem.quantity = quantity+1
	else:
		cartitem = CartItem()
		cartitem.user = request.user
		cartitem.product = book
		cartitem.itemtype = request.GET['ptype']
	cartitem.save()
	return redirect('/profile/cart')