from django import forms
from .models import *

class TaxesForm(forms.ModelForm):
	class Meta:
		model = Taxes
		fields = '__all__'

class BookForm(forms.ModelForm):
	class Meta:
		exclude = ['slug']
		model = Product
		fields = '__all__'

class CartItemForm(forms.ModelForm):
	class Meta:
		exclude = ['quantity','date_added']
		model = CartItem
		
class ShippingForm(forms.ModelForm):
	class Meta:
		model = ShippingFee
		fields = '__all__'

class OrderForm(forms.ModelForm):
	class Meta:
		model = Orders
		fields = ['courier_details','shipped_date','completed']