from django.contrib import admin
from .models import *

admin.site.register(Taxes)

class ProductAdmin(admin.ModelAdmin):
	exclude = ('slug', )
	list_display = ['name', 'slug']
admin.site.register(Product, ProductAdmin)
admin.site.register(Orders)

class CartItemAdmin(admin.ModelAdmin):
	list_display = ['product', 'user','quantity','itemtype']
admin.site.register(CartItem,CartItemAdmin)

admin.site.register(ShippingFee)