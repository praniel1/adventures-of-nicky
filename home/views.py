from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, logout
from django.shortcuts import redirect
from books.models import Product

def index(request):
	books = Product.objects.all().filter(active=True)
	return render(request, 'home/index.html',{'books':books})

def logout_view(request):
	logout(request)
	return redirect('/')